import { Events, events, Leaderboard, ScoreChangeEvent } from "./aoc"
import { askAdminForNewCookie as discordAskAdminForNewCookie, sendLeaderboard as discordSendLeaderboard, sendUserGotStar as discordSendUserGotStar } from "./discord"
import { askAdminForNewCookie, sendLeaderboard, sendUserGotStar } from "./slack"

export const assets = {
  trophyIcon: 'https://images-ext-1.discordapp.net/external/pCeZQTVVJ7m4C1wQNGuXZAi653SxHeXQcC4jP5v8s2g/https/i.imgur.com/0UOBYuw.png',
  goldStarIcon: 'https://images-ext-2.discordapp.net/external/CxFRkuVseYPbvWWe3n5WuAEIaP9wFIjDnNhKIYFwhxc/http/1.bp.blogspot.com/-AtC44SVUOOs/UqgoFqgGqTI/AAAAAAABazo/efoZJLBnUW0/s1600/gold-metal-star.png?width=1131&height=905',
  silverStarIcon: 'https://images-ext-1.discordapp.net/external/nrlp4oIJljpGKum1XB1bTWfmdByA1Rigm7DTwFml-24/https/purepng.com/public/uploads/large/purepng.com-silver-starnaturechristmasillustrationclipartvector-9615246761275pjnm.png?width=905&height=905',
  cookieIcon: 'https://www.clipartmax.com/png/full/20-206538_cookie-free-icon-http-cookie.png'
}

export const meta = {
  lastLeaderboardSent: 0
}

export async function createNotification(localLeaderboard: Leaderboard) {
  if (process.env.DISCORD_CHANNEL_ID) {
    try { await discordSendLeaderboard(localLeaderboard) }
    catch (e) {
      console.log(`Failed to send to Discord because ${e.message}`)
    }
  }
  if (process.env.SLACK_CHANNEL_ID) {
    try { await sendLeaderboard(localLeaderboard) }
    catch (e) {
      console.log(`Failed to send to slack because ${e.message}`)
    }
  }
}

export async function createUpdateNotification(scoreChange: ScoreChangeEvent) {
  if (process.env.DISCORD_CHANNEL_ID) {
    try { await discordSendUserGotStar(scoreChange) }
    catch (e) {
      console.log(`Failed to send to discord because ${e.message}`)
    }
  }
  if (process.env.SLACK_CHANNEL_ID) {
    try { await sendUserGotStar(scoreChange) }
    catch (e) {
      console.log(`Failed to send to slack because ${e.message}`)
    }
  }
}

export async function createCookieNotification() {
  if (process.env.DISCORD_CHANNEL_ID) {
    try { await discordAskAdminForNewCookie() }
    catch (e) {
      console.log(`Failed to send to discord because ${e.message}`)
    }
  }
  if (process.env.SLACK_CHANNEL_ID) {
    try { await askAdminForNewCookie() }
    catch (e) {
      console.log(`Failed to send to slack because ${e.message}`)
    }
  }
}

export function translatePosition(n: number) {
  if (n % 10 === 1 && (n > 13 || n < 11)) {
    return `${n}st`
  }

  if (n % 10 === 2 && (n > 13 || n < 11)) {
    return `${n}nd`
  }

  if (n % 10 === 3 && (n > 13 || n < 11)) {
    return `${n}rd`
  }

  return `${n}th`
}

export function registerEventHandlers() {
  events.on(Events.SCORE.toString(), (changes: ScoreChangeEvent[]) => {
    changes.forEach(o => createUpdateNotification(o))
  })

  events.on(Events.NEW_DAY.toString(), (leaderboard: Leaderboard) => {
    createNotification(leaderboard)
  })

  events.on(Events.COOKIE_EXPIRED.toString(), () => {
    createCookieNotification()
  })
}