import { getDay, getStarCountForUser, Leaderboard, ScoreChangeEvent, Star } from "./aoc";
import { setupDefaults, postDiscordMessage } from "@spacepumpkin/alterego-messenger"
import { assets, translatePosition } from "./notify";

export function sendLeaderboard(localLeaderboard: Leaderboard) {
  const content = Object.values(localLeaderboard).map(user => {
    return {
      name: translatePosition(user.position),
      value: `${user.name} with ${getStarCountForUser(user)} stars and ${user.score} points (${user.aocScore} AoC points)`
    }
  })
  return postDiscordMessage({
    title: { text: `Leaderboard [Day ${getDay()}]` },
    content: getDiscordEmbedLeaderboard(localLeaderboard),
    //thumbnail: assets.trophyIcon,
    colour: '#75FF71',
  })
}

function getDiscordEmbedLeaderboard(localLeaderboard: Leaderboard) {
  const header = underlineRow({ i: "\"Pos\"", name: "\"Alias\"", stars: "\"Stars\"", aocPoints: "\"Score\"" })
  const content = Object.values(localLeaderboard).filter(o => o.score > 0).map((o, i) => {
    return {
      i: `${o.position})`,
      name: (o.name || 'Anon').substring(0, 20),
      stars: `${getStarCountForUser(o)}`,
      //localPoints: `${o.score}`,
      aocPoints: `${o.aocScore}`
    }
  })

  let groups = []
  let group = []

  for (let i = 0; i < content.length; i++) {
    const row = content[i];

    if (group.length > 15) {
      groups.push(group)
      group = []
    }

    group.push(row)
  }

  groups.push(group)

  return groups.map(o => {
    return {
      name: '\u200B',
      value: wrapInMarkdownAndTrim('bash', formatTableRows([...header, ...o]).map(o => `${o.i} ${o.name}     ${o.stars}  ${o.aocPoints}`).join('\r\n')),
    }
  })
}

function wrapInMarkdownAndTrim(lang: string = 'md', str: string) {
  return `\`\`\`${lang}\r\n${str.substring(0, 1000).replace("'", " ")}\r\n\`\`\``
}

function underlineRow(contents: Record<string, string>) {
  return [
    contents,
    Object.keys(contents).reduce((acc, key) => {
      if (!contents[key].length) return acc
      acc[key] = "".padStart(contents[key].length + 1, "=")
      return acc
    }, {})
  ]
}

function formatTableRows(contents: Record<string, string>[]) {
  const maxLengths = contents.reduce((acc, obj: Record<string, string>) => {
    for (const key in obj) {
      if (!acc[key] || acc[key] < obj[key].length) {
        acc[key] = obj[key].length
      }
    }
    return acc
  }, {} as Record<string, number>)

  return contents.map(o => {
    for (const key in maxLengths) {
      if (!o[key]) continue
      o[key] = (o[key] as String).padEnd(maxLengths[key], " ")
    }
    return o
  })
}

export function sendUserGotStar(event: ScoreChangeEvent) {
  return postDiscordMessage({
    title: { text: `${event.name} got a ${Star[event.star]} star` },
    content: [
      { name: 'Day', value: String(event.day) },
      { name: 'Stars', value: `${event.oldStars} -> ${event.newStars}` },
      { name: 'Score', value: `${event.oldScore} -> ${event.newScore}` },
      { name: 'Position', value: `${event.oldPosition} -> ${event.newPosition}` },
      { name: 'Time Taken', value: `${event.timeTaken}` }
    ],
    thumbnail: event.star === Star.Gold ? assets.goldStarIcon : assets.silverStarIcon,
    colour: '#FFED01',
  })
}

export function askAdminForNewCookie() {
  if (!process.env.DISCORD_OWNER_ID) return
  return postDiscordMessage({
    title: { text: `Cookie expired` },
    content: [
      { name: 'Plz gib new cookie', value: `Use PUT /cookie with a plaintext body of the cookie value (session=****)` },
    ],
    discord: { user: process.env.DISCORD_OWNER_ID },
    thumbnail: assets.cookieIcon,
    colour: '#FF0000',
  })
}

export function reportError(reason: Error) {
  return postDiscordMessage({
    title: { text: `Unhandled Rejection from Advent of Code ${new Date().getFullYear()}` },
    content: [{ name: 'Message', value: reason.message }, { name: 'Stack', value: reason.stack }],
    discord: { channel: '800347837116317717' },
    colour: '#FF0000'
  })
}

export function setupDiscord() {
  setupDefaults({
    url: 'tf-services.do:9117',
    discord: { channel: process.env.DISCORD_CHANNEL_ID },
    sender: `Advent of Code ${new Date().getFullYear()}`,
    footer: {
      text: `Advent of Code ${new Date().getFullYear()}`,
      url: 'https://adventofcode.com/favicon.png'
    }
  })
}