import { App } from "@slack/bolt"

import { getDay, getStarCountForUser, Leaderboard, ScoreChangeEvent, Star, User } from "./aoc";
import { assets, translatePosition } from "./notify";

let app = null

export function sendLeaderboard(localLeaderboard: Leaderboard) {
  if (!app) return
  const textFn = (o: User) => `*${translatePosition(o.position)}*\n${o.name} with ${getStarCountForUser(o)} stars and ${o.score} points`

  const message = {
    token: process.env.SLACK_BOT_TOKEN,
    channel: process.env.SLACK_CHANNEL_ID,
    text: `Leaderboard [Day ${getDay()}]`,
    blocks: [
      {
        type: "header",
        text: {
          type: "plain_text",
          text: `Leaderboard [Day ${getDay()}]`,
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: Object.values(localLeaderboard).map(textFn).join("\n")
        },
        accessory: {
          type: "image",
          image_url: assets.trophyIcon,
          alt_text: "trophy icon"
        }
      },
      {
        type: "divider"
      },
    ]
  }

  return app.client.chat.postMessage(message)
}

export function sendUserGotStar(event: ScoreChangeEvent) {
  if (!app) return
  const obj = [
    { name: 'Day', value: String(event.day) },
    { name: 'Stars', value: `${event.oldStars} -> ${event.newStars}` },
    { name: 'Score', value: `${event.oldScore} -> ${event.newScore}` },
    { name: 'Position', value: `${event.oldPosition} -> ${event.newPosition}` },
    { name: 'Time Taken', value: `${event.timeTaken}` }
  ]

  const textFn = (o: { name: string, value: string }) => `*${o.name}*\n${o.value}`

  const message = {
    token: process.env.SLACK_BOT_TOKEN,
    channel: process.env.SLACK_CHANNEL_ID,
    text: `${event.name} got a ${Star[event.star]} star`,
    blocks: [
      {
        type: "header",
        text: {
          type: "plain_text",
          text: `${event.name} got a ${Star[event.star]} star`,
          emoji: true
        }
      },
      {
        type: "divider"
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: obj.map(textFn).join("\n")
        },
        accessory: {
          type: "image",
          image_url: event.star === Star.Gold ? assets.goldStarIcon : assets.silverStarIcon,
          alt_text: "star icon"
        }
      },
      {
        type: "divider"
      }
    ]
  }

  return app.client.chat.postMessage(message)
}

export function askAdminForNewCookie() {
  if (!process.env.SLACK_OWNER_ID || !app) return

  const obj = [
    { name: 'Plz gib new cookie', value: `Use PUT /cookie with a plaintext body of the cookie value (session=****)` }
  ]

  const textFn = (o: { name: string, value: string }) => `*${o.name}*\n${o.value}`

  const message = {
    token: process.env.SLACK_BOT_TOKEN,
    channel: process.env.SLACK_OWNER_ID,
    text: `Cookie expired`,
    blocks: [
      {
        type: "header",
        text: {
          type: "plain_text",
          text: `Cookie expired`,
          emoji: true
        }
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: obj.map(textFn).join("\n")
        },
        accessory: {
          type: "image",
          image_url: assets.cookieIcon,
          alt_text: "cookie icon"
        }
      }
    ]
  }

  return app.client.chat.postMessage(message)
}

export function setupSlack() {
  try {
    app = new App({
      token: process.env.SLACK_BOT_TOKEN,
      signingSecret: process.env.SLACK_SIGNING_SECRET,
    });
    app.error((e) => {
      console.log(`An error occured in Slack: ${e.message}`)
    })
  } catch (e) {
    console.log(`Failed to setupSlack because ${e.message}`)
  }
}