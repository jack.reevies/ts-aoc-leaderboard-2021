import polka from "polka"
import { text } from 'body-parser'
import { meta as AdventOfCode, getDay, queryLeaderboard } from "./aoc"
import { createNotification, meta as Notify } from "./notify"

export async function startServer() {
  const server = polka().use(text())

  server.get('/', (req, res) => {
    const formatted = [] as any[]
    const currentDay = getDay()
    Object.values(AdventOfCode.localLeaderboard).forEach(o => {
      formatted.push({
        name: o.name,
        score: o.score,
        stars: o.stars.filter(o => o.day <= currentDay).map(o => {
          return {
            day: o.day,
            gold: o.goldAt > 0 ? new Date(o.goldAt).toString() : false,
            silver: o.silverAt > 0 ? new Date(o.silverAt).toString() : false,
          }
        })
      })
    })
    sendJsonResponse(res, 200, {
      lastCheck: AdventOfCode.lastCheck,
      secondsCheckAgo: Math.round((Date.now() - AdventOfCode.lastCheck) / 1000),
      leaderboard: formatted
    })
  })
  server.post('/send', async (req, res) => {
    if (Date.now() - Notify.lastLeaderboardSent < 10 * 60 * 1000) {
      sendJsonResponse(res, 400, { message: 'Last leaderboard post was under 10 minutes ago - please do not spam' })
      return
    }
    
    createNotification(AdventOfCode.localLeaderboard)
    sendJsonResponse(res, 200, { message: 'ok!' })
  })

  server.put('/cookie', (req, res) => {
    if (!req.body.startsWith("session=")) {
      sendJsonResponse(res, 400, { message: 'Looks like your cookie was malformed, should look like "session=ALONGSTRING"' })
    }
    process.env.COOKIE = req.body
    sendJsonResponse(res, 200, { message: 'Thanks boi' })
  })
  server.listen(process.env.PORT || 8080)
}

function sendJsonResponse(res: any, status = 200, json: any) {
  if (!res) throw new Error('polka\'s res object was not provided as first parameter')
  const str = JSON.stringify(json)
  res.setHeader('content-type', 'application/json')
  res.statusCode = status
  res.end(str)
}