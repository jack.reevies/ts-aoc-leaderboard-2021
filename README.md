# Advent of Code leaderboard tracker

Tracks a private leaderboard and posts to discord/slack each day and when a user gets a star.

## How to use

Copy `example.env` as `.env` and fill out the information.

Both Slack and Discord are optional, omitting DISCORD_CHANNEL_ID will disable discord reporting (likewise omit SLACK_CHANNEL_ID to disable slack reporting)

LEADERBOARD_URL and COOKIE are mandatory and need to be filled in else the program will refuse to run

### Slack Integration

+ Create a new App on Slack
+ Give it the following Scopes: `chat:write`, `im:write`
+ Copy the OAuth Token (xoxb-xxxxxx) into .env
+ Find your own user ID and add itr as SLACK_OWNER_ID to get notifications when the cookie expires

## Discord Integration

+ None yet, it's locked to my own server for the time being ;D

## API

A basic API server is available with the following:

##### GET /

Get the leaderboard in JSON format

##### POST /send

Force send the leaderboard to slack/discord (cooldown of 10 minutes)

##### PUT /cookie

Update the AoC cookie. Send the new complete cookie in the body as plaintext like "session=aaaaaa11111"

## Limitations

+ This still uses a locked down discord API so DISCORD_SERVER_ID will do nothing (it is locked to my own discord server for the time being)