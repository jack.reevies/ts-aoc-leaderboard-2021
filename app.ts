import dotenv from 'dotenv';
import { pollLeaderboard } from './aoc';
import { reportError, setupDiscord } from './discord';
import { startServer } from "./httpServer";
import { registerEventHandlers } from './notify';
import { setupSlack } from './slack';
dotenv.config();

process.on('unhandledRejection', (reason: Error) => {
  reportError(reason)
  console.error(reason)
  if (reason['code'] === 'slack_webapi_platform_error') {
    // If slack creds are wrong, initial login will always throw an unhandled rejection (you can't catch it)
    return
  }
  setTimeout(process.exit, 1000 * 20)
})

async function start() {
  initialHealthCheck()
  startServer()
  if (process.env.SLACK_CHANNEL_ID && process.env.SLACK_BOT_TOKEN) {
    setupSlack()
  }
  if (process.env.DISCORD_SERVER_ID && process.env.DISCORD_CHANNEL_ID) {
    setupDiscord()
  }
  registerEventHandlers()
  pollLeaderboard()
}

function initialHealthCheck() {
  if (process.env.DISCORD_CHANNEL_ID && !process.env.DISCORD_OWNER_ID) {
    throw new Error(`DISCORD_CHANNEL_ID is set but not DISCORD_OWNER_ID. You will not recieve notifications through discord when the cookie expires`)
  }

  if (!process.env.LEADERBOARD_URL){
    throw new Error(`LEADERBOARD_URL is not set. It should look like https://adventofcode.com/20XX/leaderboard/private/view/*****`)
  }

  if (!process.env.COOKIE){
    throw new Error(`COOKIE is not set. I cannot grab private leaderboards without one. You can grab the complete cookie string from a browser request`)
  }

  if (process.env.SLACK_CHANNEL_ID && !process.env.SLACK_OWNER_ID) {
    throw new Error(`SLACK_CHANNEL_ID is set but not SLACK_OWNER_ID. You will not recieve notifications through slack when the cookie expires`)
  }
}

start()