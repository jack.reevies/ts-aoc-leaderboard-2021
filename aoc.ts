import fetch from "node-fetch"
import { EventEmitter } from "events"

const EPOCH_START = parseInt(process.env.EPOCH_START) || 1669870800000

export enum Star {
  Gold,
  Silver,
  None
}

export enum Events {
  SCORE,
  NEW_DAY,
  COOKIE_EXPIRED
}

export type Leaderboard = Record<string, User>

export interface RawJson {
  members: {
    [key: string]: {
      local_score: number
      name: string
      id: string
      last_star_ts: number
      global_score: number
      stars: number
      completion_day_level: RawJsonDayLevel
    }
  }
}

export interface RawJsonDayLevel {
  [dayNumber: string]: {
    "1"?: {
      get_star_ts: number
    },
    "2"?: {
      get_star_ts: number
    }
  }
}

export interface User {
  name: string
  position: number
  posHistory: Record<string, number>
  scoreHistory: Record<string, number>
  aocScore: number
  score: number
  stars: UserStars[],
  lastStar: number
}

export interface UserStars { day: number, goldAt: number, silverAt: number }

export interface ScoreChangeEvent {
  name: string
  oldPosition: number
  newPosition: number
  oldStars: number
  newStars: number
  oldScore: number
  newScore: number
  star: Star
  day: number
  timeTaken?: string
}

export const meta: {
  lastCheck: number
  lastCheckDay: number
  lastLeaderboardChange: number,
  localLeaderboard: Leaderboard
} = {
  lastCheck: Number.MIN_SAFE_INTEGER,
  lastCheckDay: getDay(),
  lastLeaderboardChange: Number.MIN_SAFE_INTEGER,
  localLeaderboard: {}
}

export const events = new EventEmitter()

export async function pollLeaderboard() {
  const pollWithEnvVars = () => queryLeaderboard(process.env.LEADERBOARD_URL || '', process.env.COOKIE || '')
  pollWithEnvVars()
  setInterval(pollWithEnvVars, 60 * 1000)
}

export async function queryLeaderboard(url: string, cookie: string) {
  let res = null
  let json = null
  try {
    res = await fetch(url, { redirect: 'manual', headers: { cookie } })
    json = await res.json()
  }
  catch (e) { return }

  meta.lastCheck = Date.now()

  if (res.status === 302) {
    // Failed to get leaderboard - need new cookie
    events.emit(Events.COOKIE_EXPIRED.toString())
    console.log(`Looks like the cookie has expired - got 302 trying to get leaderboard URL`)
    return
  }

  const newLeaderboard = buildLeaderboardFromJson(json)
  const updates = getChangesFromLastLeaderboard(meta.localLeaderboard, newLeaderboard)
  meta.localLeaderboard = newLeaderboard

  if (updates.length) {
    events.emit(Events.SCORE.toString(), updates)
    meta.lastLeaderboardChange = Date.now()
  }

  if (meta.lastCheckDay !== getDay()) {
    meta.lastCheckDay = getDay()
    events.emit(Events.NEW_DAY.toString(), meta.localLeaderboard)
  }

  return updates
}

function getCustomScore(user: User) {
  return (user.stars.filter(o => o.goldAt).length * 2) +
    user.stars.filter(o => o.silverAt).length
}

function buildLeaderboardFromJson(json: RawJson) {
  let leaderboard = Object.values(json.members).map(member => {
    const user = {
      name: member.name,
      aocScore: member.local_score,
      stars: dayLevelToStars(member.completion_day_level),
      position: 0,
      lastStar: member.last_star_ts * 1000
    } as User
    user.score = getCustomScore(user)
    return user
  })

  leaderboard.sort((a: User, b: User) => {
    if (a.stars.length > b.stars.length) return -1
    if (a.stars.length < b.stars.length) return 1

    if (a.score > b.score) return -1
    if (b.score < b.score) return 1

    if (a.aocScore > b.aocScore) return -1
    if (a.aocScore < b.aocScore) return 1

    return 0
  })

  leaderboard.forEach((o, i) => o.position = i + 1)
  return leaderboard.reduce((obj, cur) => {
    obj[cur.name] = cur
    return obj
  }, {}) as Leaderboard
}

function dayLevelToStars(dayLevels: RawJsonDayLevel) {
  const stars = Object.keys(dayLevels).map(dayNumber => {
    const data = dayLevels[dayNumber]
    return {
      day: Number(dayNumber),
      goldAt: data[2]?.get_star_ts * 1000 || 0,
      silverAt: data[1]?.get_star_ts * 1000 || 0
    } as UserStars
  })
  stars.sort((a, b) => a.day < b.day ? -1 : 1)
  return stars
}

function getChangesFromLastLeaderboard(localLeaderboard: Leaderboard, newLeaderboard: Leaderboard) {
  let changes: ScoreChangeEvent[] = []
  Object.values(newLeaderboard).forEach((user, i) => {
    const localUser = localLeaderboard[user.name]
    if (!localUser) return
    if (localUser.lastStar === user.lastStar) {
      return
    }
    // lastStar has changed - user must have made progress
    for (let i = 0; i < user.stars.length; i++) {
      const star = user.stars[i];
      let localStar = localUser.stars[i]
      if (star.goldAt && star.goldAt !== localStar?.goldAt) {
        changes.push(makeScoreChangeEvent(localUser, user, { star: Star.Gold, day: star.day }))
      } else if (star.silverAt && star.silverAt !== localStar?.silverAt) {
        changes.push(makeScoreChangeEvent(localUser, user, { star: Star.Silver, day: star.day }))
      }
    }
  })
  return changes
}

function makeScoreChangeEvent(localUser: User, updatedUser: User, gotStar: { star: Star, day: number }) {
  const event: ScoreChangeEvent = {
    name: updatedUser.name,
    oldPosition: localUser.position,
    newPosition: updatedUser.position,
    oldScore: localUser.score,
    newScore: updatedUser.score,
    oldStars: getStarCountForUser(localUser),
    newStars: getStarCountForUser(updatedUser),
    star: Star.None,
    day: 0,
  }

  if (gotStar) {
    const existingStar = updatedUser.stars[gotStar.day - 1]
    event.timeTaken = calculateTimeTaken(gotStar.star, existingStar.silverAt, existingStar.goldAt, gotStar.day)
    event.star = gotStar.star
    event.day = gotStar.day
  }
  return event
}

function calculateTimeTaken(star: Star, silverAt: number, goldAt: number, day: number) {
  const sinceAocDayStart = toTimestamp(Date.now() - getStartDay(day))
  if (star === Star.Silver || !silverAt) {
    return sinceAocDayStart
  }

  return `${toTimestamp(goldAt - silverAt)} (${sinceAocDayStart})`
}

export function getDay() {
  const DAY = 86400000
  return Math.ceil((Date.now() - EPOCH_START) / DAY)
}

export function getStartDay(n: number) {
  const DAY = 86400000
  return EPOCH_START + (DAY * (n - 1))
}

function toTimestamp(time: number) {
  if (time < 0) return `${time} ms`
  const days = Math.floor(time / (1000 * 60 * 60 * 24))
  time -= days * (1000 * 60 * 60 * 24)

  const hours = Math.floor(time / (1000 * 60 * 60))
  time -= hours * (1000 * 60 * 60)

  const mins = Math.floor(time / (1000 * 60))
  time -= mins * (1000 * 60)

  const seconds = Math.floor(time / (1000))
  time -= seconds * (1000)

  const pad = (n: number) => n.toString().padStart(2, '0')

  if (days > 0) {
    return `${pad(days)}d:${pad(hours)}h:${pad(mins)}m:${pad(seconds)}s`
  } else if (hours > 0) {
    return `${pad(hours)}h:${pad(mins)}m:${pad(seconds)}s`
  } else {
    return `${pad(mins)}m:${pad(seconds)}s`
  }
}

export function getStarCountForUser(user: User) {
  return user.stars.filter(o => o.goldAt).length + user.stars.filter(o => o.silverAt).length
}